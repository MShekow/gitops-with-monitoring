# GitOps for managing cluster software using GitLab, ArgoCD and Renovate Bot

See [this blog post](https://www.augmentedmind.de/2021/12/26/gitops-with-gitlab-and-monitoring/) for further details.

## Introduction

This example project illustrates how you can use [Renovate Bot](https://docs.renovatebot.com/) and
the [ArgoCD](https://argo-cd.readthedocs.io) GitOps controller to solve the following problems:

- How to keep *basic* cluster components (that every K8s cluster needs) up to date with minimal to no effort, such as an
  Ingress controller, SSL cert-bot, monitoring stack, or ArgoCD itself
- How to become alerted (from GitLab via emails!) in case ArgoCD was unable to successfully deploy automatic updates of
  these basic cluster components

The basic solution approach is this:

- We use the
  ArgoCD [App of Apps pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/) to
  maintain all applications. The `applications` folder is the Helm chart that installs the "parent" app (which you
  instruct ArgoCD to watch and install into the cluster, by manually applying the `bootstrap.yaml` file with `kubectl`
  to your cluster, which you only need to do once). That chart contains all the other apps (found
  in `applications/templates`).
- Renovate Bot will keep the ArgoCD application itself up to date, because Renovate Bot supports `Kustomize`
  configurations, and will detect once the version referenced in the `argocd-kustomize/kustomization.yaml` file becomes
  out of date. This Kustomize app is installed because it is referenced as one of the apps defined
  in `applications/templates`.
- Renovate Bot will keep all other apps up to date, because Renovate can parse ArgoCD `Application` YAML files and look
  for dependencies inside them
  (see [here](https://github.com/renovatebot/renovate/issues/7416)
  and [here](https://docs.renovatebot.com/modules/manager/argocd/)), which is explicitly configured in `renovate.json5`
  of this repo.
- For monitoring, we install a best-practice configuration of the `kube-prometheus-stack`
  [Helm chart](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack), which
  includes Prometheus, Alertmanager and Grafana, see `applications/templates/app-prom-stack.yaml`.
  See [this article](https://www.augmentedmind.de/2021/11/14/prometheus-in-kubernetes/) for background information.
    - The `argocd-kustomize/service-monitors.yaml` file ensures that Prometheus actually scrapes ArgoCD metrics.
    - The configuration values in `app-prom-stack.yaml` instruct Prometheus to detect and send alerts to Alertmanager if
      the ArgoCD *sync status* or *health status* is bad for one or more apps.
    - The `applications/templates/alertmanager-config.yaml` file instruct Alertmanager to send the received alerts to
      GitLab, using
      GitLab's [Prometheus integration](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html), which
      creates a webhook receiver that we configure Alertmanager to send *alert notifications* to.
    - GitLab turns received alerts
      to [Incident tickets](https://docs.gitlab.com/ee/operations/incident_management/incidents.html). Once an alert has
      resolved, GitLab will automatically mark the incident ticket as resolved, too.

## How to use this project

- Create a fork of this repository.
- Take a look at all YAML files in your fork and adapt values where necessary. At minimum, you need to search for
  all `https://gitlab.com/MShekow` strings and replace the repository URLs with your fork's URL!
- Adapt the `renovate.json5` file in your fork, tweaking which applications (and update types, e.g. `minor`) you want
  Renovate Bot to merge automatically.
- Create a GitLab *deploy token* ([docs](https://docs.gitlab.com/ee/user/project/deploy_tokens/)) with
  the `read_repository` scope, which you need to let ArgoCD access your repo.
- Install ArgoCD into a completely empty cluster:
    - `kubectl create ns argocd && kubectl apply -k argocd-kustomize` installs ArgoCD
    - Run `kubectl port-forward svc/argocd-server -n argocd 8080:443` (it may take a while until it works, because it
      takes some time for ArgoCD to be completely installed), open https://localhost:8080 and change the
      initially-generated admin password (see official ArgoCD Getting-started docs).
    - Open https://localhost:8080/settings/repos and register your forked repository, by clicking on *Connect repo using
      HTTPS*, using the GitLab *deploy token* (that you created above) as credentials.
- Run `kubectl apply -f bootstrap.yaml` to set up the App of Apps.
    - Observe the sync/health progress in the ArgoCD web UI. Because of a cyclic dependency, the initial sync will most
      likely not work, because the `monitoring.coreos.com/AlertmanagerConfig/argocd/argocd-alerts` fails to sync (
      because the Prometheus stack is not yet installed). As illustrated in [this screenshot](argo-cd-fix.png), you can
      solve the problem by clicking on the "Sync" button in the ArgoCD web UI to start a *manual* sync that ignores
      the `AlertmanagerConfig` temporarily.
- In GitLab, configure the *Prometheus integration* as follows:
    - Go to "Settings -> Integrations" and enable the *Prometheus integration*. Enable the **Active** checkbox, type
      some random URL for the **API URL**, and click **Save changes**.
    - Next, click on the **Visit settings page** button (or go to "Settings -> Monitor"), switch to the *Alert settings*
      tab, and enable the checkboxes *"Create an incident. Incidents are created for each alert triggered"* and
      *"Send a single email notification to Owners and Maintainers for new alerts"* and click **Save changes**.
    - Switch to the *Current integrations* tab, click the cog-wheel icon of the Prometheus integration, and switch to
      the *View credentials* tab. You should see the **Webhook URL** and **Authorization key**. If the *Authorization
      key* is empty, click on **Reset Key**.
- Apply a `Secret` of the following form to the `argocd` namespace that contains the *Authorization key* of the GitLab
  Prometheus integration:

```
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: gitlab-webhook-auth-key
data:
  authKey: <base64-encoded-authorization-key>
```

- Verify that the Alertmanager config is correct. Run the
  command `kubectl port-forward svc/alertmanager-operated -n monitoring 9093:9093` and open http://localhost:9093,
  switch to the **Status** page and verify that the shown Alertmanager configuration resembles what is expressed
  in `applications/templates/alertmanager-config.yaml`.
    - Note: it may take a few minutes after applying the above `Secret` until the Alertmanager config has updated.
- Optional (but recommended): test the Prometheus integration:
    - Create a test app (in `applications/templates`) that intentionally does not work, e.g. because you are referencing
      an invalid Helm chart repo, or a non-existent chart version.
    - Open the ArgoCD web UI to verify that ArgoCD is unable to properly sync the app.
    - After a few minutes, you should see a new incident in your forked GitLab project (and receive an email) regarding
      the ArgoCD alert.
- Invite the Renovate Bot to your repo, so that it can update your dependencies.
  See [here](https://www.augmentedmind.de/2021/07/25/renovate-bot-cheat-sheet/) for more information.

## Further customizations

You can easily add other applications that you want installed into your cluster:

- If the application is a publicly available Helm chart, add another `app-something.yaml` to `applications/templates`,
  based on the `app-nginx.yaml` file, only adapting the Helm chart repo location and chart name/version.
- If the application is one of your own applications (which lives in a *different* GitLab project), you can extend that
  project's CI pipeline with a job that clones (your fork of) *this* `gitops-with-monitoring` project, adds/updates
  a `my-app.yaml` file in `applications/templates`, and pushes the changes to this repo.

You can also tweak the `alertmanager-config.yaml` to forward more alerts to GitLab. The `kube-prometheus-stack` Helm
chart that is installed by the `applications/templates/app-prom-stack.yaml` app already preconfigures many alerts, but
by default the Alertmanager does not send any alert *notifications* to any endpoint (webhook or others).
